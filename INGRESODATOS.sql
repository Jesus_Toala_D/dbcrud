---Ingreso de datos tabla Cliente
INSERT INTO cliente VALUES('01','1315661726','Jesus Smir','Toala Delgado','2001/08/01','Calle 8 Avenida 28','0968959182');
INSERT INTO cliente VALUES('02','1308251709','Emiliano Daniel','Soledispa Catagua','1995/07/15','Calle 13 Avenida 15','0995918565');
INSERT INTO cliente VALUES('03','1305965218','Carlos Cesar','Baque Velez','2000/12/25','Calle 112 Avenida 124','0998565231');
INSERT INTO cliente VALUES('04','1319845301','Javier Frank','Insua Menoscal','1989/02/15','Calle 18 Avenida 30','0984532100');
INSERT INTO cliente VALUES('05','1305963175','Francisco Lider','Anchundia Piguave','2002/05/30','Calle 103 Avenida 109','0995412363');
---Ingreso de datos tabla Cuenta
INSERT INTO cuenta VALUES('01','01','100');
INSERT INTO cuenta VALUES('02','02','100');
INSERT INTO cuenta VALUES('03','03','100');
INSERT INTO cuenta VALUES('04','04','100');
INSERT INTO cuenta VALUES('05','05','100');
---Ingreso de datos tabla Area
INSERT INTO area VALUES('01','La Dolorosa','Calle 9','Avenida 22');
INSERT INTO area VALUES('02','Santa Martha','Calle 12','Avenida 30');
INSERT INTO area VALUES('03','Los Esteros','Calle 110','Avenida 114');
INSERT INTO area VALUES('04','Santa Monica','Calle 19','Avenida 26');
--- Ingreso de datos tabla Personal_Administrativo
INSERT INTO personal_administrativo VALUES('01','1305961238','Teresa Belen','Bustamante Pin','Calle 13 Avenida 26','28','0996512020');
INSERT INTO personal_administrativo VALUES('02','1315963201','Sebastian Gregorio','Torres Palacios','Calle 30 Avenida 9','39','0989541239');
INSERT INTO personal_administrativo VALUES('03','1305961238','Maria Jose','Santana Mendoza','Calle 7 Avenida 17','36','0995156314');
---Ingreso de datos tabla Moto
INSERT INTO moto VALUES('01','Honda','Roja','250','Mash');
INSERT INTO moto VALUES('02','Suzuki','Negra','125','Mash');
INSERT INTO moto VALUES('03','Kawasaki','Amarilla','300','Mash');
INSERT INTO moto VALUES('04','Daytona','Negra','250','Mash');
INSERT INTO moto VALUES('05','Honda','Azul','180','Mash');
---Ingreso de datos tabla Personal_Motorizado
INSERT INTO personal_motorizado VALUES('01','05','1309562136','Franklin Jose','Bustamante Vargas','0954852132','C','2021/10/30');
INSERT INTO personal_motorizado VALUES('02','04','1315623165','Jose Edwin','Perlaza Quininde','0995412956','C','2021/10/23');
INSERT INTO personal_motorizado VALUES('03','03','1304530121','Erwin Erick','Peralta Vinces','0989651202','C','2021/11/12');
INSERT INTO personal_motorizado VALUES('04','02','1305478962','Byron Octavio','Quimiz Salvatierra','0961203678','C','2022/10/13');
INSERT INTO personal_motorizado VALUES('05','01','1315478963','Leonel Jonathan','Cruz Martinez','0984123648','C','2022/04/09');
---Ingreso de datos tabla Revision
INSERT INTO revision VALUES('01','01','05','Bueno','0','2021/05/30');
INSERT INTO revision VALUES('02','02','04','Dañada','1','2021/05/30');
INSERT INTO revision VALUES('03','03','03','Bueno','0','2021/05/30');
INSERT INTO revision VALUES('04','04','02','Dañada','2','2021/05/30');
INSERT INTO revision VALUES('05','05','01','Dañada','2','2021/05/30');
INSERT INTO revision VALUES('06','01','05','Bueno','1','2021/08/27');
INSERT INTO revision VALUES('07','02','04','Dañada','3','2021/08/27');
INSERT INTO revision VALUES('08','03','03','Bueno','0','2021/08/27');
INSERT INTO revision VALUES('09','04','02','Bueno','0','2021/08/27');
INSERT INTO revision VALUES('10','05','01','Dañada','2','2021/08/27');
---Ingreso de datos tabla Solicitud
INSERT INTO solicitud VALUES('01','02','02','04','2020/12/12','Rechazado','Desconocido');
INSERT INTO solicitud VALUES('02','04','03','03','2020/12/25','Rechazado','Desconocido');
INSERT INTO solicitud VALUES('03','04','02','02','2021/01/25','Confirmado','Ropa');
INSERT INTO solicitud VALUES('04','02','01','03','2021/01/29','Confirmado','Alimentos');
INSERT INTO solicitud VALUES('05','01','03','01','2021/02/13','Rechazado','Desconocido');
INSERT INTO solicitud VALUES('06','03','03','01','2021/02/25','Confirmado','Alimento');
INSERT INTO solicitud VALUES('07','05','01','04','2021/03/10','Confirmado','Bebida');
INSERT INTO solicitud VALUES('08','02','02','03','2021/03/28','Rechazado','Desconocido');
INSERT INTO solicitud VALUES('09','03','02','01','2021/03/31','Confirmado','Bebidas');
INSERT INTO solicitud VALUES('10','04','02','02','2021/04/05','Confirmado','Ropa');
INSERT INTO solicitud VALUES('11','05','01','03','2021/04/17','Confirmado','Alimento');
---Ingreso de datos tabla Entrega
INSERT INTO entrega VALUES('01','03','04','02','04','2021/01/25','1','3','Entregado');
INSERT INTO entrega VALUES('02','04','03','05','02','2021/01/29','2','2','Entregado');
INSERT INTO entrega VALUES('03','06','01','03','03','2021/02/25','1','2','Entregado');
INSERT INTO entrega VALUES('04','07','04','01','05','2021/03/10','1','1','Entregado');
INSERT INTO entrega VALUES('05','09','01','04','03','2021/03/31','2','3','Entregado');
INSERT INTO entrega VALUES('06','10','02','04','04','2021/04/05','1','2','Entregado');
INSERT INTO entrega VALUES('07','11','03','02','05','2021/04/17','1','1','Entregado');
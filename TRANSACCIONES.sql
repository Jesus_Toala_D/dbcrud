--transaccion 1
CREATE OR REPLACE PROCEDURE public.transaccionoficial(
	character varying,
	character varying,
	character varying,
	character varying,
	character varying,
	date,
	double precision,
	double precision)
LANGUAGE 'plpgsql'
AS $BODY$
declare
    estado character varying;
    begin
insert into entrega(cod_entrega, cod_solicitud, cod_area, cod_motorizado, cod_cliente, fecha_entrega, peso_entrega,cantidad_productos) values ($1, $2, $3, $4, $5, $6, $7, $8);
select estado_solicitud into estado from solicitud where solicitud.cod_solicitud=$2;
            if (estado='Confirmado') then
  update entrega set estado_entrega='Entregado' where entrega.cod_entrega=$1;
  else
          raise exception 'Ingreso no permitido por codigo de solicitud rechazada';
		          rollback;
            end if;
    commit;
end;
$BODY$;

---transaccion2
CREATE OR REPLACE PROCEDURE public.transaccionoficial2(
	character varying,
	character varying,
	character varying)
LANGUAGE 'plpgsql'
AS $BODY$
declare
    subt double precision;
	ivaa double precision;
	tot double precision;
    peso double precision;
	cantidad double precision;
	mont double precision;
	comi double precision;
    begin
insert into factura(cod_factura, cod_entrega, cod_cuenta) values ($1, $2, $3);
select peso_entrega into peso from entrega where entrega.cod_entrega=$2;
comi:=4;
subt:= peso*4;
ivaa:= subt*0.12;
tot:= subt+ivaa+comi;
select monto into mont from cuenta where cuenta.cod_cuenta=$3;
if (tot <= mont) then
update factura set subtotal=subt where factura.cod_cuenta=$3;
update factura set comision=comi where factura.cod_cuenta=$3;
update factura set iva=ivaa where factura.cod_cuenta=$3;
update factura set total=tot where factura.cod_cuenta=$3; 
update cuenta set monto=monto-tot where cuenta.cod_cuenta=$3; 
else 
 raise exception 'Ingreso no permitido de factura por no poseer suficiente dinero en la cuenta';
		          rollback;
end if;
    commit;
end;
$BODY$;
